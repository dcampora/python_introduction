{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Track reconstruction made easy\n",
    "==============================\n",
    "\n",
    "This is a pet project to do track reconstruction,\n",
    "based on real data coming from the LHCb detector at CERN.\n",
    "\n",
    "Think you can make it better? Go ahead and try!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is track reconstruction?\n",
    "-----------------------------\n",
    "\n",
    "At the LHCb detector, millions of particles collide at speeds\n",
    "close to the speed of light, leaving traces (hits) on the sensors\n",
    "placed in their way.\n",
    "\n",
    "The collisions that happen at the same time are packed\n",
    "into an *event*, and sent to one of our servers,\n",
    "that must reconstruct the tracks that formed each particle\n",
    "in real time.\n",
    "\n",
    "This project contains events in json format. These events are\n",
    "then processed by some reconstruction algorithm, and finally\n",
    "the results are validated. That is, the particles found by\n",
    "the solver are matched against the real particles that came out of\n",
    "the collisions in the event.\n",
    "\n",
    "![velopix reconstruction example](reco_example.png \"velopix reconstruction example\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Diving into details\n",
    "-------------------\n",
    "\n",
    "Input files are specified in json. An *event model* to parse them\n",
    "is shipped with this project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import event_model as em\n",
    "import json\n",
    "f = open(\"velojson/1.json\")\n",
    "json_data = json.loads(f.read())\n",
    "event = em.event(json_data)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The LHCb Velopix detector has 52 modules. Spread across the modules,\n",
    "we should have many hits, depending on the event we are on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(len(event.modules))\n",
    "print(len(event.hits))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hits are composed of an ID, and {x, y, z} coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(event.hits[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Modules are placed at some z in the detector. Each module\n",
    "may have as many hits as particles crossed by it, plus some noise to\n",
    "make things interesting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(event.modules[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visually, the data looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from visual.base import print_event_2d\n",
    "%matplotlib inline\n",
    "\n",
    "print_event_2d(event)\n",
    "print_event_2d(event, y=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A tracking algorithm solution\n",
    "----------------\n",
    "\n",
    "A simplistic implementation runs through all sensors sequentially,\n",
    "finding tracks by matching hits in a straight line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from search_by_triplet_trie import search_by_triplet_trie\n",
    "tracks = search_by_triplet_trie().solve(event)\n",
    "\n",
    "print(len(tracks))\n",
    "print(tracks[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we should validate these results, and we'll look\n",
    "at three things:\n",
    "    \n",
    "*   Reconstruction Efficiency: The fraction of real particles we have reconstructed.\n",
    "    > \\# correctly reconstructed / \\# real tracks\n",
    "\n",
    "*   Clone Tracks: Tracks that are similar to other correctly reconstructed tracks.\n",
    "    > \\# clone tracks / \\# correctly reconstructed\n",
    "\n",
    "*   Fake Tracks: Tracks that are incorrect, either created by noise or by incorrectly reconstructing a track.\n",
    "    > \\# incorrectly reconstructed / \\# all reconstructed\n",
    "\n",
    "We will get the validation detailed for different kinds of particles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import validator_lite as vl\n",
    "vl.validate_print([json_data], [tracks])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And visually, we can see what our solution looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print_event_2d(event, tracks=tracks)\n",
    "print_event_2d(event, tracks=tracks, y=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Data analysis exercises\n",
    "---------------\n",
    "\n",
    "We will just try to understand how the data of the problem looks like. Produce a plot with:\n",
    "\n",
    "* X axis: Module number\n",
    "* Y axis: Number of hits in the module\n",
    "\n",
    "Usually, algorithms start looking at data from the last module, because the number of hits is lower there. Is this true?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The technique used in the above solution, named \"search_by_triplet_trie\", finds triplets of hits in neighbouring modules on the same side. Odd modules are placed on the left, whereas even modules are placed on the right.\n",
    "\n",
    "* Fetch all hits in a numpy array. Use a selector to iterate over the hits on modules on either side.\n",
    "* Plot the number of pairs of hits in every consecutive pair of modules on the same side.\n",
    "* Plot the number of triplets of hits in every consecutive triplet of modules on the same side."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\"search_by_triplet_trie\" obtains many tracks, aka collections of signals left in the detector by particles. We would like to know the goodness of these tracks:\n",
    "\n",
    "* Iterate over the tracks from the solution. Print for every track hits its X, Y and Z coordinates.\n",
    "* Create a means_squared_error method that gets a track as an input and calculates the MSE (https://en.wikipedia.org/wiki/Mean_squared_error) of a track.\n",
    "* The above algorithm gets a high fake rate, and this is most likely due to tracks with a high MSE. Make a track filter to remove the bad tracks and pass that to the validator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
